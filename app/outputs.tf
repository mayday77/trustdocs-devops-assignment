output "lambda" {
  value = aws_lambda_function.this
}

output "apigateway" {
  value = aws_api_gateway_rest_api.this
}

output "route53" {
  value = aws_route53_zone.this.name
}